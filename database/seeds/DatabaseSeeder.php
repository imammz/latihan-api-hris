<?php

use Illuminate\Database\Seeder;;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        ini_set('memory_limit','500M');

        $faker = Faker::create();
        DB::table('users')->insert([
            'nama_pegawai' => $faker->name,
            'nik_pegawai' => $faker->unique()->creditCardNumber,
            'status_online' => '0',
            'email' => $faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => bcrypt('secret'), // passwrod secret
            'api_token' => str_random(40),
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),

        ]);

        for($i=0;$i<=20;$i++) {
            $user = DB::select('select id_user from users ORDER BY RAND() LIMIT 1');
            $user = $user[0];

            $year = rand(2009, 2018);
            $month = rand(1, 12);
            $day = rand(1, 28);
            $date = Carbon::create($year,$month ,$day , 0, 0, 0);


            $faker = Faker::create();
             DB::table('presensi')->insert([
            'tanggal' => $date->format('Y-m-d'),
            'jam_masuk' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
            'jam_pulang' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
            'id_user' => $user->id_user,
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        }

        for($i=0;$i<=20;$i++) {
            $user = DB::select('select id_user from users ORDER BY RAND() LIMIT 1');
            $user = $user[0];

            $year = rand(2009, 2018);
            $month = rand(1, 12);
            $day = rand(1, 28);
            $date = Carbon::create($year,$month ,$day , 0, 0, 0);

            $faker = Faker::create();
             DB::table('agenda')->insert([
            'judul_agenda' => $faker->name,
            'isi_agenda' => $faker->realText,
            'tanggal_agenda' => $date->format('Y-m-d'),
            'id_user' => $user->id_user,
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        }

        for($i=0;$i<=20;$i++) {
            $user = DB::select('select id_user from users ORDER BY RAND() LIMIT 1');
            $user = $user[0];

            $year = rand(2009, 2018);
            $month = rand(1, 12);
            $day = rand(1, 28);
            $date = Carbon::create($year,$month ,$day , 0, 0, 0);

            $faker = Faker::create();
             DB::table('surat_masuk')->insert([
            'judul_surat' => $faker->name,
            'tanggal_surat' => $date->format('Y-m-d'),
            'isi_surat' => $faker->realText,
            'nomor_surat' => rand(10,99).'/sm/'.rand(1,12).'/'.rand(2011,2018),
            'tindak_lanjut' => '-',
            'status_balas' => '0',
            'id_user' => $user->id_user,
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        }

    }
}
