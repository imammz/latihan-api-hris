<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SuratMasukController extends GeneralController
{
    public function index(Request $request) {

        $user = $this->_getUser($request);

        $id = $user->id_user;

        $response = [];
        $data = DB::table('surat_masuk')->where('id_user',$id)
        ->orderBy('id_surat_masuk', 'desc')->get();


        if(count($data)>0) {
            $response['message'] = 'Berhasil';
            $response['jml'] = count($data);
            $response['data'] = $data;
        }
        else {
            $response['message'] = 'Tidak Ditemukan';
        }

        return response()->json($response);

    }


    public function detail(Request $request,$id_surat_masuk) {

        $user = $this->_getUser($request);

        $id = $user->id_user;

        $response = [];
        $data = DB::table('surat_masuk')->where('id_user',$id)->where('id_surat_masuk',$id_surat_masuk)->first();


        if(count($data)>0) {
            $response['message'] = 'Berhasil';
            $response['data'] = $data;
        }
        else {
            $response['message'] = 'Tidak Ditemukan';
        }

        return response()->json($response);

    }


    public function prosesTl(Request $request) {

        $user = $this->_getUser($request);

        $id = $user->id_user;
        $id_surat_masuk = $request->id_surat_masuk;

        $response = [];
        $data = DB::table('surat_masuk')->where('id_user',$id)->where('id_surat_masuk',$id_surat_masuk)
        ->update(
            ['tindak_lanjut'=>$request->tindak_lanjut,
        'status_balas'=>"1",
        'updated_at'=>date('Y-m-d H:i:s')]);


        if(count($data)>0) {
            $response['message'] = 'Berhasil';
            $response['data'] = $data;
        }
        else {
            $response['message'] = 'Tidak Ditemukan';
        }

        return response()->json($response);

    }


}
