<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;

class MobileController extends GeneralController
{

    public function login(Request $request) {

    $email = (isset($request->email))?$request->email:'';
    $password = (isset($request->password))?$request->password:'';

    $response = [];

    if($email=='' or $password =='') {
        $response['message'] = 'Email dan Password Harus Diisi';
        $response['login'] = false;

    }
    else {
        $cek = DB::select("select id_user,api_token,password from users where
         email = '{$email}' ");

         if(count($cek)>0) {
             $user = $cek[0];

             if (Hash::check($password,$user->password)) {
                $response['message'] = 'Berhasil Login';
                $response['login'] = true;
                $response['api_token'] = $user->api_token;
            }
            else {
                $response['message'] = 'Password  Salah';
                $response['login'] = false;
            }
         }
         else {
            $response['message'] = 'Email  Salah';
            $response['login'] = false;
         }
    }

    return response()->json($response);

    }


    public function ubahStatus(Request $request) {
        $response = [];

        $user = $this->_getUser($request);

        $id = $user->id_user;

        if( !isset($request->online)) {
            $response['message'] = 'parameter online harus tersedia';
        }
        else {

            if($request->online==true) {
                $request->online = "1";
            }
            if($request->online==false) {
                $request->online = "0";
            }

            if($request->online=="1" or $request->online=="0") {
                $response['message'] = 'Berhasil';
                DB::table('users')->where('id_user',$id)
                ->update(['status_online'=>$request->online,'updated_at'=>date('Y-m-d H:i:s')]);

            }
            else {
                $response['message'] = 'Format status online = "1" atau "0"';
            }

        }

        return response()->json($response);
    }

}
