<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AgendaController extends GeneralController
{
    public function index(Request $request) {

        $user = $this->_getUser($request);

        $id = $user->id_user;

        $response = [];
        $data = DB::table('agenda')->where('id_user',$id)
        ->orderBy('id_agenda', 'desc')->get();


        if(count($data)>0) {
            $response['message'] = 'Berhasil';
            $response['jml'] = count($data);
            $response['data'] = $data;
        }
        else {
            $response['message'] = 'Tidak Ditemukan';
        }

        return response()->json($response);

    }


    public function detail(Request $request,$id_agenda) {

        $user = $this->_getUser($request);

        $id = $user->id_user;

        $response = [];
        $data = DB::table('agenda')->where('id_user',$id)->where('id_agenda',$id_agenda)->first();


        if(count($data)>0) {
            $response['message'] = 'Berhasil';
            $response['data'] = $data;
        }
        else {
            $response['message'] = 'Tidak Ditemukan';
        }

        return response()->json($response);

    }

    public function store(Request $request) {
        $user = $this->_getUser($request);

        $id = $user->id_user;

           if( isset($request->judul_agenda) and
               isset($request->isi_agenda) and
                isset($request->tanggal_agenda)) {

                $data = [];
                $data['judul_agenda']   =$request->judul_agenda;
                $data['isi_agenda']=$request->isi_agenda;
                $data['tanggal_agenda'] =$request->tanggal_agenda;
                $data['id_user'] =$user->id_user;
                $data['created_at'] =date("Y-m-d H:i:s");

                DB::table('agenda')->insert($data);
                $response['message'] = 'Berhasil';
        }

        else {
            $response['message'] = 'Judul Agenda, Isi Agenda, Tanggal Agenda harus tersedia';
        }

        return response()->json($response);
    }


    public function update(Request $request) {
        $user = $this->_getUser($request);

        $id = $user->id_user;

           if( isset($request->judul_agenda) and
               isset($request->isi_agenda) and
                isset($request->tanggal_agenda) and
                isset($request->id_agenda)) {

                $data = [];
                $data['judul_agenda']   =$request->judul_agenda;
                $data['isi_agenda']=$request->isi_agenda;
                $data['tanggal_agenda'] =$request->tanggal_agenda;
                $data['id_user'] =$user->id_user;
                $data['updated_at'] =date("Y-m-d H:i:s");

                DB::table('agenda')->where('id_agenda',$request->id_agenda)->update($data);
                $response['message'] = 'Berhasil';
        }

        else {
            $response['message'] = 'Id Agenda, Judul Agenda, Isi Agenda, Tanggal Agenda harus tersedia';
        }
        return response()->json($response);
    }

    public function destroy(Request $request) {
        $user = $this->_getUser($request);
        $id = $user->id_user;

           if( isset($request->id_agenda)) {
                DB::table('agenda')->where('id_agenda',$request->id_agenda)->delete();
                $response['message'] = 'Berhasil';
           }

        else {
            $response['message'] = 'Id Agenda harus tersedia';
        }

        return response()->json($response);
    }


}
