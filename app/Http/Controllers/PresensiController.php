<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PresensiController extends GeneralController
{
    //
    public function index(Request $request) {

        $user = $this->_getUser($request);

        $id = $user->id_user;

        $response = [];
        $data = DB::table('presensi')->where('id_user',$id)
        ->orderBy('id_presensi', 'desc')->get();


        if(count($data)>0) {
            $response['message'] = 'Berhasil';
            $response['jml'] = count($data);
            $response['data'] = $data;
        }
        else {
            $response['message'] = 'Tidak Ditemukan';
        }

        return response()->json($response);

    }
}
