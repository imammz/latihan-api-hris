<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GeneralController extends Controller
{
    //

    public function _getUser($request) {

        $token = str_replace('Bearer ','',$request->header('Authorization'));

        $user = DB::table('users')->where('api_token',$token)->first();

       return $user;

    }

}
