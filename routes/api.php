<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//  Routing khusus api


Route::middleware('auth:api')->get('/user', function (Request $request) {

    $token = str_replace('Bearer ','',$request->header('Authorization'));
    $user = DB::table('users')->where('api_token',$token)->first();

     if($user->status_online=="1") {
        $user->status_online = true;
     }
     elseif($user->status_online=="0") {
        $user->status_online = false;
     }

    return response()->json($user);
});

Route::get('/test', function () {
    echo '<h1>ini melalui method GET</h1>';
});

Route::put('/test/{id}', function ($id) {
    echo '<h1>ini melalui method PUT mengakses ID : '.$id.'</h1>';
});

Route::post('/test', function () {
    echo '<h1>ini melalui method POST</h1>';
});

Route::delete('/test/{id}', function ($id) {
    echo '<h1>ini melalui method DELETE mengakses ID : '.$id.'</h1>';
});


Route::post('/login','MobileController@login');

/*
  param :  email, password
*/


/*
  param :  online (bolean)
*/
Route::get('/presensi','PresensiController@index')
->middleware('auth:api');

Route::get('/surat-masuk','SuratMasukController@index')
->middleware('auth:api');
Route::get('/surat-masuk/{id}','SuratMasukController@detail')
->middleware('auth:api');
Route::put('/surat-masuk/tindak-lanjut',
'SuratMasukController@prosesTl')
->middleware('auth:api');

Route::get('/agenda','AgendaController@index')
->middleware('auth:api');
Route::get('/agenda/{id}','AgendaController@detail')
->middleware('auth:api');
Route::post('/agenda','AgendaController@store')
->middleware('auth:api');
Route::put('/agenda','AgendaController@update')
->middleware('auth:api');
Route::delete('/agenda','AgendaController@destroy')
->middleware('auth:api');


